import React from 'react';
import './App.css';
import RSuiteSelect from "./RSuiteSelect";
import ReactDropdownTreeSelect from "./ReactDropdownTreeSelect";
import AntdTreeSelect from "./AntdTreeSelect";
import "antd/dist/antd.css";
import ReactSelect from "./ReactSelect";

function App() {
    return (
        <div className="App">
            <header>
                <h1>Tree Component POC</h1>
                <p>This application tests various tree selector components in their support for multiple parents of an option.</p>
                <p>In addition, React Select's support for multiple occurrences of the same option is tested as well.</p>
            <hr/>
            </header>
            <section>
                <RSuiteSelect/>
            </section>
            <section>
                <ReactDropdownTreeSelect/>
            </section>
            <section>
                <AntdTreeSelect/>
            </section>
            <section>
                <ReactSelect/>
            </section>
        </div>
    );
}

export default App;
