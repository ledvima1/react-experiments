import data from "./tree-data.json";
import DropdownTreeSelect from "react-dropdown-tree-select";
import "react-dropdown-tree-select/dist/styles.css";
import _ from "lodash";

const ReactDropdownTreeSelect = () => {
    const options = _.cloneDeep(data);
    return <div className="mt-1">
        <label className="mr-1">
            <a href="https://github.com/dowjones/react-dropdown-tree-select">React Dropdown Tree Select</a>
        </label>
        <DropdownTreeSelect
            data={options}
            className="bootstrap-demo"
        />
        <hr/>
    </div>;
};

export default ReactDropdownTreeSelect;
