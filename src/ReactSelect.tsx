import data from "./list-data.json";
import _ from "lodash";
import Select from "react-select";

const ReactSelect = () => {
    const options = _.cloneDeep(data);

    return <div className="mt-1">
        <label className="mr-1">
            <a href="https://react-select.com/home">React Select</a>
        </label>
        <Select
            className="basic-single"
            classNamePrefix="select"
            name="color"
            isClearable={true}
            isSearchable={true}
            isMulti={true}
            hideSelectedOptions={false}
            options={options}
        />
        <hr/>
    </div>
};

export default ReactSelect;
