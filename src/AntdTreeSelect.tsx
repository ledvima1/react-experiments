import data from "./tree-data.json";
import _ from "lodash";
import {TreeSelect} from "antd";

const AntdTreeSelect = () => {
    const options = _.cloneDeep(data);

    return <div className="mt-1">
        <label className="mr-1">
            <a href="https://ant.design/components/tree-select/">Ant Design TreeSelect</a>
        </label>
        <TreeSelect
            showSearch
            style={{ width: '100%' }}
            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
            placeholder="Select"
            allowClear
            multiple
            treeDefaultExpandAll
            treeData={options}
            fieldNames={{ label: "label", value: "value", children: "children" }}
        />
        <hr/>
    </div>;
};

export default AntdTreeSelect;
