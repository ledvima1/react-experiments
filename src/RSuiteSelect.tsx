import data from "./tree-data.json";
import {CheckTreePicker} from "rsuite";
import _ from "lodash";

const RSuiteSelect = () => {
    const options = _.cloneDeep(data);
    return <div className="mt-1">
        <label className="mr-1">
            <a href="https://rsuitejs.com/components/check-tree-picker/">RSuite CheckTreePicker</a>
        </label>
        <CheckTreePicker
            defaultExpandAll
            data={options}
            appearance="default"
            placeholder="Select"
            style={{width: 280}}
        />
        <hr/>
    </div>;
};

export default RSuiteSelect;
